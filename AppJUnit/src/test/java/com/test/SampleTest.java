package com.test;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class SampleTest {
	
	//non static data using object
	//static data using class name
	
	Sample obj;
	
	static String msg;
	
	@BeforeAll
	public static void beforeClass()
	{
		System.out.println("before class");
		
		msg = "hi hello";
	}
	
	@AfterAll
	public static void afterClass()
	{
		System.out.println("after class");
		
		msg = null;
	}
	
	@BeforeEach
	public void setUp()
	{
		System.out.println("before test method");
		
		obj = new Sample();
	}
	
	@AfterEach
	public void setDown()
	{
		System.out.println("after test method");
		
		obj = null;
	}
	
	@Test
	public void testAdd()
	{
		System.out.println("test add method");
		
		assertEquals(100, obj.add(40, 60));
	}
	@Test
	public void testSub()
	{
		System.out.println("test sub method");
		
		assertEquals(20, obj.sub(50, 30));
	}
	@Test
	public void testMul()
	{
		System.out.println("test mul method");
		
		assertEquals(50, obj.mul(25, 2));
	}
	
	@Test
	public void testSayHello()
	{
		System.out.println("test hello method");
		
		assertEquals("hi hello", obj.sayHello(msg));
	}

}
