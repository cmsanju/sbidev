package com.cls;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

public class Exp1 {
	
	public static void main(String[] args) {
		
		//List data = new ArrayList();
		
		List data = new LinkedList();
		
		data.add(10);
		data.add("java");
		data.add(34.56f);
		data.add('A');
		data.add("java");
		data.add(false);
		data.add(45.90);
		
		System.out.println(data);
		
		data.set(4, "hello");
		
		System.out.println(data);
		
		data.remove(5);
		
		Iterator itr = data.iterator();
		
		while(itr.hasNext())
		{
			System.out.println(itr.next());
		}
				
	}

}
