package com.cls;

import java.util.TreeSet;

public class Exp3 {
	
	public static void main(String[] args) {
		
		TreeSet<Integer> data = new TreeSet<Integer>();
		
		data.add(20);
		data.add(2);
		data.add(45);
		data.add(83);
		data.add(5);
		data.add(1);
		data.add(3);
		data.add(7);
		data.add(20);
		
		System.out.println(data);
		
		
		TreeSet<String> dat = new TreeSet<String>();
		
		dat.add("lenovo");
		dat.add("dell");
		dat.add("asus");
		dat.add("sony");
		dat.add("apple");
		dat.add("vitu");
		
		System.out.println(dat.ceiling("apple"));
	}

}
