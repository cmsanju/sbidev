package com.cls;

import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map.Entry;
import java.util.TreeMap;

public class Exp4 {
	
	public static void main(String[] args) {
		
		HashMap<String, Integer> data = new HashMap<String, Integer>();
		
		data.put("lenovo", 200);
		data.put("sony", 500);
		data.put("dell", 200);
		data.put("asus", 40);
		data.put("apple", 300);
		data.put("mac", 400);
		data.put("apple", 500);
		
		System.out.println(data);
		
		
		LinkedHashMap<String, Integer> data1 = new LinkedHashMap<String, Integer>();
		
		data1.put("lenovo", 200);
		data1.put("sony", 500);
		data1.put("dell", 200);
		data1.put("asus", 40);
		data1.put("apple", 300);
		data1.put("mac", 400);
		data1.put("apple", 500);
		
		System.out.println(data1);
		
		
		Iterator<Entry<String, Integer>> itr = data1.entrySet().iterator();
		
		while(itr.hasNext())
		{
			Entry<String, Integer> et = itr.next();
			
			System.out.println(et.getKey()+" "+et.getValue());
		}
		
		for(String ky : data1.keySet())
		{
			System.out.println(ky+" "+data1.get(ky));
		}
		
		TreeMap<String, Integer> data2 = new TreeMap<String, Integer>();
		
		data2.put("lenovo", 200);
		data2.put("sony", 500);
		data2.put("dell", 200);
		data2.put("asus", 40);
		data2.put("apple", 300);
		data2.put("mac", 400);
		data2.put("apple", 500);
		
		System.out.println(data2);
		
		for(String ky : data2.keySet())
		{
			System.out.println(ky+" "+data2.get(ky));
		}
	}
}
