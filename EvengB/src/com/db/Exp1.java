package com.db;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;

public class Exp1 {
	
	public static void main(String[] args) throws Exception
	{
		
		//1 load the driver class
		Class.forName("com.mysql.jdbc.Driver");
		
		//2 create connection object
		Connection con = DriverManager.getConnection("jdbc:mysql://localhost:3306/xyz", "root", "password");
		
		//3 create statement object
		Statement stmt = con.createStatement();
		
		//4 execute query
		//stmt.execute("create table student(id int, name varchar(50), city varchar(50))");
		
		String sql1 = "insert into student values(2, 'Spring', 'bLR')";
		
		//String sql = "update student set name = 'Hibernate', city = 'Hyd' where id = 2";
		
		//String sql = "delete from student where id = 2";
		
		stmt.execute(sql1);
		
		String sql = "select * from student";
		
		ResultSet rs = stmt.executeQuery(sql);
		
		while(rs.next())
		{
			System.out.println(rs.getInt(1)+" "+rs.getString(2)+" "+rs.getString(3));
		}
		
		System.out.println("Done.");
		
		//5 close the connection object
		
		con.close();
	}

}
