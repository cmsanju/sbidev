package com.db;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;

public class Exp2 {
	
	public static void main(String[] args) throws Exception
	{
		
		Class.forName("com.mysql.jdbc.Driver");
		
		Connection con = DriverManager.getConnection("jdbc:mysql://localhost:3306/xyz", "root", "password");
		/*
		PreparedStatement pst = con.prepareStatement("insert into student values(?,?,?)");
		
		pst.setInt(1, 101);
		pst.setString(2, "Apple");
		pst.setString(3, "TPT");
		
		
		PreparedStatement pst = con.prepareStatement("update student set name = ? where id = ?");
		
		pst.setString(1, "Lenovo");
		pst.setInt(2, 101);
		
		pst.execute();
		*/
		
		PreparedStatement pst = con.prepareStatement("delete from student where id= ?");
		
		pst.setInt(1, 101);
		
		pst.execute();
		
		System.out.println("Done.");
		
		con.close();
	}

}
