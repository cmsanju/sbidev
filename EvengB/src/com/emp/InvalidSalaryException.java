package com.emp;

public class InvalidSalaryException extends Exception
{
	public InvalidSalaryException(String msg)
	{
		super(msg);
	}
}
