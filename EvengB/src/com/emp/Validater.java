package com.emp;

public class Validater {
	
	public void checkSal(double sal)throws ValidSalaryException,InvalidSalaryException
	{
		if(sal > 0)
		{
			ValidSalaryException ve = new ValidSalaryException("valid salary");
			
			throw(ve);
		}
		else
		{
			InvalidSalaryException ie = new InvalidSalaryException("invalid salary");
			
			throw(ie);
		}
	}

}
