package com.excp;

public class Exp1 {
	
	public static void main(String[] args) {
		
		try {
		
			System.out.println(10/2);
		
			String name = "java";
		
			System.out.println(name.charAt(2));
		
			int[] ar = {12,34,56};
		
			System.out.println(ar[5]);
		}
		catch(ArithmeticException ae)
		{
			System.out.println("don't enter zero for den");
		}
		catch(StringIndexOutOfBoundsException sie)
		{
			System.out.println("check your name length");
		}
		catch(ArrayIndexOutOfBoundsException aie)
		{
			System.out.println("check your array size");
		}
		catch(Exception e)
		{
			System.out.println("check your inputs");
		}
		finally
		{
			System.out.println("i am from finally");
		}
		
	}

}
//try
//catch
//finally
//throws
//throw