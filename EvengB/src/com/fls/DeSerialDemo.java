package com.fls;

import java.io.FileInputStream;
import java.io.ObjectInputStream;

public class DeSerialDemo {
	
	public static void main(String[] args) throws Exception
	{
		
		FileInputStream fis = new FileInputStream("src/employee.txt");
		
		ObjectInputStream bis = new ObjectInputStream(fis);
		
		Employee e = (Employee)bis.readObject();
		
		System.out.println(e.id+" "+e.name+" "+e.city+" "+e.pin);
	}

}
