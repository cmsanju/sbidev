package com.fls;

import java.io.File;
import java.io.FileOutputStream;

public class FileWrite {
	
	public static void main(String[] args) throws Exception
	{
		
		File file = new File("src/write.txt");
		
		FileOutputStream fw = new FileOutputStream(file);
		
		String msg = "Hi this is simple byte stream write operation";
		
		fw.write(msg.getBytes());
		
		System.out.println("Done.");
	}

}
