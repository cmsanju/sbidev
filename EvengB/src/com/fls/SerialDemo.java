package com.fls;

import java.io.FileOutputStream;
import java.io.ObjectOutputStream;

public class SerialDemo 
{
	public static void main(String[] args)throws Exception
	{
		
		FileOutputStream fos = new FileOutputStream("src/employee.txt");
		
		ObjectOutputStream fw = new ObjectOutputStream(fos);
		
		Employee e = new Employee();
		
		e.id = 123;
		e.name = "Hello";
		e.city = "MPL";
		e.pin = 234234;
		
		fw.writeObject(e);
		
		System.out.println("Done.");
	}
}
