package com.test;

interface I1
{
	void cat();
	
	default void dog()
	{
		
	}
	
	static void fox()
	{
		
	}
}

interface I2
{
	void show();
}

interface I3 extends I1,I2
{
	void human();
}

class Impl1 implements I3
{
	public void show()
	{
		System.out.println("i2 overrided");
	}
	
	public void cat()
	{
		System.out.println("I1 overrided");
	}
	
	public void human()
	{
		System.out.println("i3 overrided");
	}
}

public class Exp38 {
	
	public static void main(String[] args) {
		
		Impl1 obj = new Impl1();
		
		obj.cat();
		obj.dog();
		obj.human();
		obj.show();
	}

}
