 package com.test;

 interface X
 {
	 void add();
	 
	 interface Y
	 {
		 void sub();
	 }
 }
 
 class Impl2 implements X.Y
 {
	 public void sub()
	 {
		 System.out.println("overrided");
	 }
	 
 }
 
public class Exp39 {
	
	public static void main(String[] args) {
		
		Impl2 obj = new Impl2();
		
		obj.sub();
	}

}
