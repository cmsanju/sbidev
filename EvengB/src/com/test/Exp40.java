package com.test;

//functional interface :: inside the interface only one abstract method is there that interface is F I

@FunctionalInterface
interface FunInf
{
	void draw();
	
	default void m1()
	{
		System.out.println("default");
	}
}

public class Exp40 {
	
	public static void main(String[] args) {
		
		FunInf obj = () -> System.out.println("overrided");
		
		obj.draw();
		obj.m1();
	}

}

//inside the interface there is no physical data but it will provide runtime behaviour to your
//implemented class
/*
 * marker or tagged interfaces 
 * 
 * java.lang.Cloneable
 * java.io.Serializable
 * java.rmi.Remote
 * 
 */

