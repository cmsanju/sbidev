package com.test;

public class Exp41 {
	
	public static void main(String[] args) {
		
		String val = "100";
		
		int x = Integer.parseInt(val);
		
		
		int y = 300;
		
		String str = String.valueOf(y);
		
		double d = 456.68;
		
		String str1 = String.valueOf(d);
		
		
		// auto boxing 
		
		int i1 = 90;
		
		Integer i2 = new Integer(i1);
		
		double d1 = 838.383;
		
		Double d2 = new Double(d1);
		
		
		//auto un-boxing
		
		Float f1 = new Float(45.44);
		
		float f2 = f1;
		
		Integer i3 = new Integer(400);
		
		int i4 = i3;
	}

}
