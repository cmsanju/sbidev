package com.test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Test {
	
	public void m1()
	{
		System.out.println("m1");
		
		m2();
	}
	
	public void m2()
	{
		System.out.println("m2");
	}
	
	public static void main(String[] args) {
		
		List<Integer> dt= Arrays.asList(2,3,4,56);
		
		dt.stream().map(td -> td*2);
		
		System.out.println(dt);
	}

}
