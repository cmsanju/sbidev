package com.ths;

public class Exp1 extends Thread
{
	@Override
	public void run()
	{
		System.out.println("run method");
	}
	
	public static void main(String[] args) {
		
		Exp1 t1 = new Exp1();
		
		Exp1 t2 = new Exp1();
		
		Exp1 t3 = new Exp1();
		
		System.out.println("Default thread name : "+t1.getName());
		System.out.println("Default thread name : "+t2.getName());
		System.out.println("Default thread name : "+t3.getName());
		
		t1.setName("Transfer");
		t2.setName("Withdraw");
		t3.setName("Credit");
		
		System.out.println("after setting thread name : "+t1.getName());
		System.out.println("after setting thread name : "+t2.getName());
		System.out.println("after setting thread name : "+t3.getName());
		
		System.out.println("Default thread priority : "+t1.getPriority());
		System.out.println("Default thread priority : "+t2.getPriority());
		System.out.println("Default thread priority : "+t3.getPriority());
		
		System.out.println(MAX_PRIORITY);
		System.out.println(NORM_PRIORITY);
		System.out.println(MIN_PRIORITY);
		
		t1.setPriority(MAX_PRIORITY);
		
		System.out.println(t1.getPriority());
	}

}
