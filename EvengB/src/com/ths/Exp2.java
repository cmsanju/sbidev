package com.ths;

class Add
{
	public void add()
	{
		System.out.println(120+20);
	}
}
class Sub
{
	public void sub()
	{
		System.out.println(30-40);
	}
}

public class Exp2 implements Runnable
{

	@Override
	public void run() {
		
		System.out.println("run method");
		
		try
		{
			Add obj1 = new Add();
			
			obj1.add();
			
			Thread.sleep(2000);
			
			Sub obj2 = new Sub();
			
			obj2.sub();
		}
		catch (Exception e) {
			
			e.printStackTrace();
		}
		
	}
	
	public static void main(String[] args) {
		
		Exp2 t1 = new Exp2();
		
		//t1.start();
		
		ThreadGroup tg1 = new ThreadGroup("Bank");
		
		Thread t2 = new Thread(tg1, t1, "Transfer");//converting Runnable interface object into Thread class
		Thread t3 = new Thread(tg1, t1, "Withdraw");
		Thread t4 = new Thread(tg1, t1, "Credit");
		
		ThreadGroup tg2 = new ThreadGroup("Maths");
		
		Thread t5 = new Thread(tg2, t1, "Add");//converting Runnable interface object into Thread class
		Thread t6 = new Thread(tg2, t1, "Div");
		Thread t7 = new Thread(tg2, t1, "Sub");
		
		t2.start();
		System.out.println(tg1.activeCount());
		System.out.println(tg2.activeCount());
		
	}
	
}
