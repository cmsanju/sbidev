package com.ths;

public class LifeCycle extends Thread
{
	@Override
	public void run()
	{
		try {
			
			Thread.sleep(2000);
			System.out.println("i am from run()");
			
			
		}
		catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	
	public static void main(String[] args) throws Exception
	{
		
		LifeCycle t1 = new LifeCycle();
		
		System.out.println("Before starting thread state : "+t1.getState());
		System.out.println("Before starting thread status : "+t1.isAlive());
		
		t1.start();
		
		System.out.println("after startng thread state : "+t1.getState());
		System.out.println("after starting thread status : "+t1.isAlive());
		
		Thread.sleep(500);
		
		System.out.println("in sleeep thread state : "+t1.getState());
		System.out.println("in sleep thread status : "+t1.isAlive());
		
		t1.join();
		
		System.out.println("after joining thread state : "+t1.getState());
		System.out.println("after joining thread status : "+t1.isAlive());
	}
}
