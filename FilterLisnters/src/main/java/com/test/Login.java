package com.test;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;


@WebServlet("/Login")
public class Login extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		response.setContentType("text/html");
		
		PrintWriter out = response.getWriter();
		
		String name = request.getParameter("user");
		
		HttpSession session = request.getSession();
		
		session.setAttribute("info", name);
		
		ServletContext ctx = getServletContext();
		
		int x = (int)ctx.getAttribute("totalu");
		int y = (int)ctx.getAttribute("currentu");
		
		out.println("UserName : "+name);
		out.println("<br>Total users : "+x);
		out.println("<br> Current users : "+y);
		
		out.println("<br> <a href = 'Servlet2'>log out</a>");
	}

}
