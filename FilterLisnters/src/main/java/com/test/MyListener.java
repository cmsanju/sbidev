package com.test;

import javax.servlet.ServletContext;
import javax.servlet.annotation.WebListener;
import javax.servlet.http.HttpSessionEvent;
import javax.servlet.http.HttpSessionListener;


@WebListener
public class MyListener implements HttpSessionListener {

    public static int current,total = 0;
    
    public static ServletContext ctx = null;
    
    public void sessionCreated(HttpSessionEvent se)  { 
        
    	total++;
    	current++;
    	
    	ctx = se.getSession().getServletContext();
    	
    	ctx.setAttribute("totalu", total);
    	ctx.setAttribute("currentu", current);
    }

	
    public void sessionDestroyed(HttpSessionEvent se)  { 
         
    	current--;
    	
    	ctx.setAttribute("currentu", current);
    }
	
}
