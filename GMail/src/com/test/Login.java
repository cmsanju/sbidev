package com.test;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


@WebServlet("/Login")
public class Login extends HttpServlet {
	private static final long serialVersionUID = 1L;
   
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
			
		response.setContentType("text/html");
		
		PrintWriter out = response.getWriter();
		
		String name = request.getParameter("user");
		String pass = request.getParameter("pwd");
		
		User obj = new User();
		
		obj.setUserName(name);
		obj.setPassword(pass);
		
		if(obj.getUserName().equals("admin") && obj.getPassword().equals("admin"))
		{
			//out.println("login success.");
			
			response.sendRedirect("https://www.google.com/");
		}
		else
		{
			//out.println("login failed.");
			
			out.println("<font color = 'red'>invalid username and password</font>");
			
			RequestDispatcher rd = request.getRequestDispatcher("login.html");
			rd.include(request, response);
		}
			
	}

}
