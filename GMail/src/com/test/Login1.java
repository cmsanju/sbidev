package com.test;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;


@WebServlet("/Login1")
public class Login1 extends HttpServlet {
	private static final long serialVersionUID = 1L;
   
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		response.setContentType("text/html");
		
		PrintWriter out = response.getWriter();
		
		String u = request.getParameter("user");
		
		//Cookie ck = new Cookie("info", u);
		
		//response.addCookie(ck);
		
		HttpSession session = request.getSession();
		
		session.setAttribute("info", u);
		
		out.print("form servlet1 : "+u);
		
		out.println("<a href = 'Servlet2'> go to next </a>");
	}

}
