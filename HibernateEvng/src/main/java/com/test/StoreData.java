package com.test;

import java.nio.channels.SeekableByteChannel;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;

public class StoreData {
	
	public static void main(String[] args) {
		
		Configuration cfg = new Configuration();
		
		cfg.configure("hibernate.cfg.xml");
		
		SessionFactory sf = cfg.buildSessionFactory();//SECOND LEVEL CACHE
		
		Session session = sf.openSession();//FIRST LEVEL CACHE
		
		Transaction t = session.beginTransaction();
		
		Employee emp = new Employee();//TRANSIENT
		
		emp.setEmp_name("Hero");
		emp.setEmp_dsg("Tester");
		
		session.persist(emp);//PERSISTENCE
		
		t.commit();
		
		Employee emp1 = session.get(Employee.class, 1);
		
		System.out.println(emp1.getId()+" "+emp1.getEmp_name()+" "+emp1.getEmp_dsg());
		
		Employee emp2 = session.get(Employee.class, 2);
		
		System.out.println(emp2.getId()+" "+emp2.getEmp_name()+" "+emp2.getEmp_dsg());
		
		System.out.println("Done.");
		
		session.clear();//DETACHED
	}

}
