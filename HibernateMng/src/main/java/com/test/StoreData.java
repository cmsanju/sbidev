package com.test;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;

public class StoreData {
	
	public static void main(String[] args) {
		
		Configuration cfg = new Configuration();
		
		cfg.configure("hibernate.cfg.xml");
		
		SessionFactory sf = cfg.buildSessionFactory();
		
		Session session = sf.openSession();
		
		Transaction t = session.beginTransaction();
		
		Student obj = new Student();
		
		obj.setStd_name("Naman");
		obj.setStd_clg("BITS");
		
		session.persist(obj);
		
		t.commit();
		
		
		Student dt = (Student) session.get(Student.class, 1);
		
		System.out.println(dt.getId()+" "+dt.getStd_name()+" "+dt.getStd_clg());
		
		System.out.println("Done.");
	}
}
