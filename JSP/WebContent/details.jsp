<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
</head>
<body>
		<jsp:useBean id="emp" class="com.test.Employee"></jsp:useBean>
		
		<jsp:setProperty property="*" name="emp"/>
		
		<jsp:getProperty property="id" name="emp"/>
		<jsp:getProperty property="name" name="emp"/>
		<jsp:getProperty property="city" name="emp"/>
</body>
</html>