<%@page import="java.util.Date"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
</head>
<body>
		<h1>WELCOME TO JSP</h1>
		
		<!-- DECLARATION TAG -->
		
			<%!
			
				int x = 40;
				int y = 50;
				
				public int add()
				{
					return x+y;
				}
			
			%>
			
			<!-- expression tag -->
			
			<%= add() %>
			
			<!-- scriplet tag -->
			
			<%
				Date dt = new Date();
			
			  out.println("Current Date: "+dt);
			%>
</body>
</html>