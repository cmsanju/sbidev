package com.test;

public class Sample {
	
	public int add(int x, int y)//x=20 y = 30
	{
		return x+y;
	}
	
	public int sub(int x, int y)
	{
		return x-y;
	}
	
	public int mul(int x, int y)
	{
		return x*y;
	}
	
	public String sayHello(String msg)
	{
		return msg;
	}
}
