package com.test;

import static org.junit.Assert.assertEquals;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

public class SampleTest {
	
	Sample obj;
	
	@BeforeClass
	public static void beforeAll()
	{
		System.out.println("Before  class");
	}
	
	@AfterClass
	public static void afterAll()
	{
		System.out.println("After class");
	}
	
	@Before
	public void setUp()
	{
		System.out.println("before test method");
		
		obj = new Sample();
	}
	
	@After
	public void setDown()
	{
		System.out.println("after test method");
		
		obj = null;
	}
	
	@Test
	public void testAdd()
	{
		System.out.println("add test method");
		
		assertEquals(50, obj.add(20, 30));
		
	}
	@Test
	public void testSub()
	{
		System.out.println("test sub method");
		
		assertEquals(30, obj.sub(50, 20));
	}
	@Test
	public void testMul()
	{
		System.out.println("test mul method");
		
		assertEquals(4, obj.mul(2, 2));
	}
	@Test
	public void testSayHello()
	{
		System.out.println("test greet method");
		
		assertEquals("hi hello", obj.sayHello("hi hello"));
		
	}
	

}
