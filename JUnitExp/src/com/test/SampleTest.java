package com.test;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class SampleTest {
	
	Sample obj;
	
	@BeforeAll
	public static void beforeAll()
	{
		System.out.println("before all test cases");
	}
	
	@AfterAll
	public static void afterAll()
	{
		System.out.println("after all the test cases");
	}
	
	@BeforeEach
	public void setUp()
	{
		System.out.println("before test method");
		
		obj = new Sample();
	}
	
	@AfterEach
	public void setDown()
	{
		System.out.println("after test method");
		
		obj = null;
	}
	
	@Test
	public void testAdd()
	{
		//int x = obj.add(20, 30);
		System.out.println("add method");
		assertEquals(50, obj.add(20, 30));
	}
	
	@Test
	public void testSub()
	{
		System.out.println("sub method");
		assertEquals(30, obj.sub(50, 20));
	}
	
	@Test
	public void testMul()
	{
		System.out.println("mul method");
		assertEquals(80, obj.mul(40, 2));
	}
	
	@Test
	public void testSayHello()
	{
		System.out.println("string method");
		assertEquals("hi hello", obj.sayHello("hi hello"));
		
	}

}
