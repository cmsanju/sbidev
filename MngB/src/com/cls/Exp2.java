package com.cls;

import java.util.ListIterator;
import java.util.Stack;

public class Exp2 {
	
	public static void main(String[] args) {
		
		Stack data = new Stack<>();
		
		data.add(10);
		data.add("java");
		data.add(34.56f);
		data.add(34.44);
		data.add('C');
		data.add("java");
		data.add(false);
		
		ListIterator ltr = data.listIterator();
		
		
		System.out.println(data);
		
		System.out.println(data.peek());
		
		data.push("hello");
		
		System.out.println(data);
		
		System.out.println(data.peek());
		
		System.out.println(data.pop());
		
		System.out.println(data.search(101));
		
		data.clear();
		
		System.out.println(data.empty());
	}
}
