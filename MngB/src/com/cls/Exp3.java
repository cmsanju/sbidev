package com.cls;

import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.ListIterator;
import java.util.Set;

public class Exp3 {
	
	public static void main(String[] args) {
		
		Set data = new HashSet<>();
		
		data.add(10);
		data.add("java");
		data.add(34.56f);
		data.add(34.44);
		data.add('C');
		data.add("java");
		data.add(false);
		data.add(10);
		
		System.out.println(data);
		
		Set data1 = new LinkedHashSet<>();
		
		data1.add(10);
		data1.add("java");
		data1.add(34.56f);
		data1.add(34.44);
		data1.add('C');
		data1.add("java");
		data1.add(false);
		data1.add(10);
		
		System.out.println(data1);
		
		Iterator itr = data1.iterator();
		
		
		
		while(itr.hasNext())
		{
			System.out.println(itr.next());
		}
	}

}
