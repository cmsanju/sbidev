package com.cls;

import java.util.Set;
import java.util.TreeSet;

public class Exp4 {
	
	public static void main(String[] args) {
		
		Set<Integer> data = new TreeSet<>();
		
		data.add(11);
		data.add(9);
		data.add(15);
		data.add(5);
		data.add(1);
		data.add(10);
		data.add(7);
		
		
		System.out.println(data);
		
		Set<String> data1 = new TreeSet<String>();
		
		data1.add("hello");
		data1.add("lenovo");
		data1.add("asus");
		data1.add("sony");
		data1.add("apple");
		data1.add("java");
		
		System.out.println(data1);
		
		
	}

}
