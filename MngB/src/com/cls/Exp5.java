package com.cls;

import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.TreeMap;

public class Exp5 {
	
	public static void main(String[] args) {
		
		Map<String, Integer> data = new TreeMap<>();
		
		data.put("lenovo", 340);
		data.put("sony", 500);
		data.put("asus", 230);
		data.put("lenovo", 400);
		data.put("apple", 500);
		data.put("mac", 690);
		data.put("dell", 150);
		
		System.out.println(data);
		
		
		Iterator<Entry<String, Integer>> itr = data.entrySet().iterator();
		
		while(itr.hasNext())
		{
			Entry<String, Integer> et = itr.next();
			
			System.out.println(et.getKey()+" : "+et.getValue());
		}
		
		for(String ky : data.keySet())
		{
			System.out.println(ky+" : "+data.get(ky));
		}
		
		//data.containsKey();
		
	}
}
