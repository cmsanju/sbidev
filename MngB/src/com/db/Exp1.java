package com.db;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;

public class Exp1 {
	
	public static void main(String[] args) throws Exception
	{
		
		//1 load the driver class
		Class.forName("com.mysql.jdbc.Driver");
		
		//2 create connection object
		Connection con = DriverManager.getConnection("jdbc:mysql://localhost:3306/xxyy", "root", "password");
		
		//3 create statement object
		Statement stmt = con.createStatement();
		
		//4 execute query
		//stmt.execute("create table emp1(id int, name varchar(50), city varchar(50))");
		
		String sql1 = "insert into emp1 values (5, 'Spring', 'Blr')";
		
		String sql3 = "update emp1 set name = 'Hibernate', city = 'Hyd' where id = 5";
		
		String sql2 = "delete from emp1 where id = 4";
		
		stmt.addBatch(sql1);
		stmt.addBatch(sql3);
		stmt.addBatch(sql2);
		
		stmt.executeBatch();
		
		String sql = "select * from emp1";
		
		ResultSet rs = stmt.executeQuery(sql);
		
		while(rs.next())
		{
			System.out.println("ID : "+rs.getInt(1)+" Name : "+rs.getString(2)+" City : "+rs.getString(3));
		}
		
		System.out.println("Done.");
		
		//5 close the connection object
		con.close();
		
	}

}
