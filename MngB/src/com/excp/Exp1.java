package com.excp;

import java.util.Scanner;

public class Exp1 {
	public static void main(String[] args) {
		
		Scanner sc = null;
		
		try {
			Class.forName("");
			System.out.println("Hello");
		
			System.out.println(200/2);//
		
			System.out.println("Java");
			
			int[] ar = {10,20,30,40};
			
			System.out.println(ar[3]);
			
			String name = "java";
			
			System.out.println(name.charAt(2));
			
			String str = null;
			
			System.out.println(str.charAt(0));
			
			sc = new Scanner(System.in);
		}
		
		//System.out.println();
		catch(ArithmeticException e)
		{
			System.out.println("don't enter zero for den");
		}
		catch(ArrayIndexOutOfBoundsException aie)
		{
			System.out.println("check your arr size");
		}
		catch(StringIndexOutOfBoundsException sie)
		{
			System.out.println("check your name length");
		}
		catch(Exception e)
		{
			//System.out.println("check your inputs");
			//System.exit(0);
			
			
		}
		
		//System.out.println();
		finally
		{
			System.out.println("i am from finally");
			try {
			sc.close();
			}
			catch(Exception e)
			{
				System.out.println(e.getMessage());
				
				System.out.println(e);
				
				e.printStackTrace();
			}
		}
	}
}
