package com.fls;

import java.io.FileWriter;

public class CharWrite {
	
	public static void main(String[] args) throws Exception
	{
		
		FileWriter fw = new FileWriter("src/sample.txt");
		
		String msg = "Hi this is write operation using char stream";
		
		fw.write(msg);
		
		fw.flush();
		
		System.out.println("Done.");
	}

}
