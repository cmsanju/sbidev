package com.fls;

import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

public class SerialDemo {
	
	public static void main(String[] args) throws Exception
	{
		
		FileOutputStream fos = new FileOutputStream("src/employee.txt");
		
		ObjectOutputStream os = new ObjectOutputStream(fos);
		
		Employee emp = new Employee();
		
		emp.id = 111;
		emp.name = "Hello";
		emp.city = "MPL";
		emp.pcode = 123123;
		
		os.writeObject(emp);
		
		System.out.println("Done.");
	}

}
