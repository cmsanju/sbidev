package com.test;

interface B
{
	void human();
	
	default void show()
	{
		System.out.println("default method");
	}
	
	static void disp()
	{
		System.out.println("static method");
	}
}

abstract class C
{
	public void cat()
	{
		System.out.println("abs normal method");
	}
	
	public abstract void dog();
	
}

class A extends C implements B
{
	public  void animal()
	{
		System.out.println("norm class");
	}
	
	@Override
	public void dog()
	{
		System.out.println("abs overrided");
	}
	@Override
	public void human()
	{
		System.out.println("inf overrided");
	}
}

public class Exp1 {
	
	public static void main(String[] args) {
		
		A a = new A();
		
		a.animal();
		a.dog();
		a.human();
		a.show();
		
		B.disp();
	}
}
