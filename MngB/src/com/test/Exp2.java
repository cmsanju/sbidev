package com.test;

interface I1
{
	void add();
	
	interface I2
	{
	
		void sub();
	}
}

class I1impl implements I1.I2
{
	@Override
	public void sub()
	{
		System.out.println("inner interface method");
	}
}

public class Exp2 
{
	public static void main(String[] args) {
		
		I1impl obj = new I1impl();
		
		obj.sub();
	}
}
