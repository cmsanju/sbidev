package com.test;

@FunctionalInterface
interface FunInf //inside the interface only one abstract method
{
	 String greet();
	 
}

public class Exp3 {
	
	public static void main(String[] args) {
		
		FunInf obj = new FunInf()
				{
					@Override
					public String greet()
					{
						System.out.println("overrided");
						
						return "hello";
					}
				};
				
				obj.greet();
				
		new FunInf()
		{
			@Override
			public String greet()
			{
				System.out.println("nameless");
				
				return "test";
			}
		}.greet();
		
		//from jdk 8 onwards
		
		FunInf x = () ->{
			
			System.out.println("lambda expression");
			
			return "JDK 8";
		};
		
		x.greet();
	}

}
