package com.test;

public class Exp4 {
	
	public static void main(String[] args) {
		
		int x = 30;
		
		double y = 595;
		
		String v = String.valueOf(y);
		
		//Auto boxing
		
		int j = 33;
		
		Integer i = new Integer(j);
		
		float f = 848.33f;
		
		Float ff = new Float(f);
		
		//AUTO un-boxing
		
		Double dd = new Double(733.83);
		
		double d = dd;
	}

}
