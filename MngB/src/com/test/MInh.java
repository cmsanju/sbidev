package com.test;

interface H
{
	void dog();
}

interface I
{
	void cat();
}

interface J extends H,I
{
	void ant();
}

class K implements H,I,J
{

	@Override
	public void cat() {
		
		
	}

	@Override
	public void dog() {
		
		
	}

	@Override
	public void ant() {
		
		
	}
	
}

public class MInh {
	
	public static void main(String[] args) {
		
		
	}

}
