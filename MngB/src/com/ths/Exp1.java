package com.ths;

class Add
{
	public void add()
	{
		System.out.println("add method : "+(34+44));
	}
}

class Sub
{
	public void sub()
	{
		System.out.println("sub method : "+(46-30));
	}
}

public class Exp1 extends Thread
{
	@Override
	public void run()
	{
		try
		{
			Sub obj1 = new Sub();
			obj1.sub();
			Thread.sleep(2000);
			Add obj2 = new Add();
			obj2.add();
			
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
	}
	
	public static void main(String[] args) {
		
		Exp1 t1 = new Exp1();
		Exp1 t2 = new Exp1();
		Exp1 t3 = new Exp1();
		Exp1 t4 = new Exp1();
		
		System.out.println("Default thread name : "+t1.getName());
		System.out.println("Default thread name : "+t2.getName());
		System.out.println("Default thread name : "+t3.getName());
		System.out.println("Default thread name : "+t4.getName());
		
		t1.setName("add");
		t2.setName("sub");
		t3.setName("transfer");
		t4.setName("withdraw");
		
		System.out.println("after setting thread name : "+t1.getName());
		System.out.println(" thread name : "+t2.getName());
		System.out.println(" thread name : "+t3.getName());
		System.out.println(" thread name : "+t4.getName());
		
		System.out.println(MAX_PRIORITY);
		System.out.println(NORM_PRIORITY);
		System.out.println(MIN_PRIORITY);
		
		System.out.println("Default thread priority : "+t1.getPriority());
		System.out.println("Default thread priority : "+t2.getPriority());
		System.out.println("Default thread priority : "+t3.getPriority());
		System.out.println("Default thread priority : "+t4.getPriority());
		
		t1.setPriority(MAX_PRIORITY);
		t4.setPriority(MIN_PRIORITY);
		
		System.out.println("after setting thread priority : "+t1.getPriority());
		System.out.println("after setting thread priority : "+t4.getPriority());
		
		t1.start();
	}

}
