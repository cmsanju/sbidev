package com.ths;

public class Exp2 implements Runnable
{
	@Override
	public void run()
	{
		System.out.println("i am from run() : "+Thread.currentThread().getName());
	}
	
	public static void main(String[] args) {
		
		Exp2 t1 = new Exp2();
		
		//t1.start();
		
		ThreadGroup tg1 = new ThreadGroup("Banking");
		
		Thread t2 = new Thread(tg1, t1, "Transfer");
		Thread t3 = new Thread(tg1, t1, "Withdraw");
		Thread t4 = new Thread(tg1, t1, "Credit");
		
		ThreadGroup tg2 = new ThreadGroup("Maths");
		
		Thread t5 = new Thread(tg2, t1, "Add");
		Thread t6 = new Thread(tg2, t1, "Sub");
		Thread t7 = new Thread(tg2, t1, "Div");
		
		t2.start();
		t4.start();
		System.out.println("TG1 : "+tg1.activeCount());
		t6.start();
		t7.start();
		System.out.println("TG2 : "+tg2.activeCount());
	}

}
