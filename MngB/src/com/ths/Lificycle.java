package com.ths;

public class Lificycle extends Thread
{
	@Override
	public void run()
	{
		try {
			Thread.sleep(2000);
			System.out.println("i am from run()");
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
	}
	
	public static void main(String[] args) throws Exception
	{
		
		Lificycle t1 = new Lificycle();
		
		System.out.println("Before start thread state : "+t1.getState());
		System.out.println("Before start thread status : "+t1.isAlive());
		
		t1.start();
		System.out.println("after start thread state : "+t1.getState());
		System.out.println("after start thread status : "+t1.isAlive());
		
		Thread.sleep(1000);
		
		System.out.println("in sleep thread state : "+t1.getState());
		System.out.println("in sleep thread status : "+t1.isAlive());
		
		t1.join();
		
		System.out.println("after joining thread state : "+t1.getState());
		System.out.println("after joining thread status : "+t1.isAlive());
		
	}
}
