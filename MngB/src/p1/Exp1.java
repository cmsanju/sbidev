package p1;

public class Exp1 {
	
	private int a = 200;
	        int b = 300;
	protected int c = 400;
	public int d = 500;
	
	
	public void disp()
	{
		System.out.println(a);
		System.out.println(b);
		System.out.println(c);
		System.out.println(d);
	}
	
	public static void main(String[] args) {
		
		Exp1 e = new Exp1();
		e.disp();
	}

}

class Exp2 extends Exp1
{
	public void disp()
	{
		//System.out.println(a);
		
		System.out.println(b);
		System.out.println(c);
		System.out.println(d);
	}
}


