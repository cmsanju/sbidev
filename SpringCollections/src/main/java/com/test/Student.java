package com.test;

import java.util.List;

public class Student {
	
	private int id;
	
	private String name;
	
	private List<String> course;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public List<String> getCourse() {
		return course;
	}

	public void setCourse(List<String> course) {
		this.course = course;
	}
	
	public void disp()
	{
		System.out.println("ID : "+id+" Name : "+name);
		
		for(String crs : course)
		{
			System.out.println(crs);
		}
	}

}
