package com.test;

import org.springframework.beans.factory.annotation.Autowired;

public class Product {
	
	private int id;
	
	private String name;
	
	private String price;
	
	@Autowired
	private Address adr;
	
	public Product()
	{
		
	}
	
	public Product(int id, String name, String price, Address adr)
	{
		this.id = id;
		this.name = name;
		this.price = price;
		this.adr = adr;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getPrice() {
		return price;
	}

	public void setPrice(String price) {
		this.price = price;
	}
	

	public Address getAdr() {
		return adr;
	}

	public void setAdr(Address adr) {
		this.adr = adr;
	}
	
	public void disp()
	{
		System.out.println("ID : "+id+" Name : "+name+" Price : "+price);
		
		adr.details();
	}
}
