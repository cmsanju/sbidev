package com.test;

import org.springframework.beans.factory.BeanFactory;
import org.springframework.beans.factory.xml.XmlBeanFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;

public class Test {
	
	public static void main(String[] args) {
		
		//Resource rs = new ClassPathResource("beans.xml");
		
		//BeanFactory bn = new XmlBeanFactory(rs);
		
		ApplicationContext ctx = new ClassPathXmlApplicationContext("beans.xml");
		
		Customer c1 = (Customer)ctx.getBean("cst");
		Customer c2 = (Customer)ctx.getBean("cst1");
		
		c1.disp();
		c2.disp();
		
		ApplicationContext ct = new AnnotationConfigApplicationContext(ConfigCustom.class);
		
		Customer obj = (Customer)ct.getBean(Customer.class);
		
		obj.setId(444);
		obj.setName("Hero");
		
		System.out.println(obj.getId()+" "+obj.getName());
	}

}
