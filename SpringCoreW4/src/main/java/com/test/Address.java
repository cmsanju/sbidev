package com.test;

public class Address {
	
	private String city;
	
	private String state;
	
	private String cntry;
	
	
	
	public Address()
	{
		
	}
	
	public Address(String city, String state, String cntry)
	{
		this.city = city;
		this.state = state;
		this.cntry = cntry;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getCntry() {
		return cntry;
	}

	public void setCntry(String cntry) {
		this.cntry = cntry;
	}
	
	public void details()
	{
		System.out.println(city+" "+state+" "+cntry);
	}

}
