package com.test;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class ConfigTest {
	
	@Bean
	public Customer getCustomer()
	{
		return new Customer();
	}
	
	@Bean
	public Address getAddress()
	{
		return new Address();
	}
	
	@Bean
	public HelloTest getObj()
	{
		return new HelloTest();
	}

}
