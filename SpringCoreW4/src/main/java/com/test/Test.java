package com.test;

import org.springframework.beans.factory.BeanFactory;
import org.springframework.beans.factory.xml.XmlBeanFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;

public class Test {
	
	public static void main(String[] args) {
		
		//Resource rs = new ClassPathResource("beans.xml");
		
		//BeanFactory bn = new XmlBeanFactory(rs);
		
		ApplicationContext ctx = new ClassPathXmlApplicationContext("beans.xml");
		
		Customer c = (Customer)ctx.getBean("cst");
		
		c.disp();
		
		Customer c1 = (Customer)ctx.getBean("cst1");
		
		c1.disp();
		
		ApplicationContext ctx1 = new AnnotationConfigApplicationContext(ConfigTest.class);
				
		HelloTest t = (HelloTest)ctx1.getBean(HelloTest.class);	
		
		t.greetUser();
	}

}
