package com.test;

import org.springframework.jdbc.core.JdbcTemplate;

public class EmpDao {
	
	private JdbcTemplate jdbcTemplate;

	public void setJdbcTemplate(JdbcTemplate jdbcTemplate) {
		this.jdbcTemplate = jdbcTemplate;
	}
	
	public int saveEmp(Employee emp)
	{
		String sql = "insert into emp values ("+emp.getId()+",'"+emp.getName()+"',"+emp.getSalary()+")";
		
		return jdbcTemplate.update(sql);
	}
	
	public int updateEmployee(Employee emp)
	{
		String sql = "update emp set name = '"+emp.getName()+"', salary = "+emp.getSalary()+" where id ="+emp.getId()+"";
		
		return jdbcTemplate.update(sql);
	}
	
	public int deleteEmployee(Employee emp)
	{
		String sql = "delete from emp where id = "+emp.getId()+" ";
		
		return jdbcTemplate.update(sql);
	}
}
