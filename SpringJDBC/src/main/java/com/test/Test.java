package com.test;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class Test {
	
	public static void main(String[] args) {
		
		ApplicationContext ctx = new ClassPathXmlApplicationContext("beans.xml");
		
		EmpDao e = (EmpDao)ctx.getBean("edao");
		
		Employee emp = new Employee();
		
		emp.setId(102);
		emp.setName("Spring");
		emp.setSalary(3345.55);
		
		//e.saveEmp(emp);
		
		//e.updateEmployee(emp);
		
		e.deleteEmployee(emp);
		
		System.out.println("Done.");
	}
}
