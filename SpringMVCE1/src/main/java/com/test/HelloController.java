package com.test;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class HelloController {
	
	long l = 300l;
	
	int c = (int)l;
		
	@RequestMapping("/wuser")
	public String greetUser()
	{
		return "greet";
	}
	
	@RequestMapping("/greetagain")
	public String greetAgain()
	{
		return "again";
	}
}//Spring Boot + Hibernate + Rest API + MySQL / H2 
