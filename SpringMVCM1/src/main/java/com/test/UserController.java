package com.test;

import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class UserController {
	
	@RequestMapping("/ulogin")
	public String userPage()
	{
		return "login";
	}
	
	@RequestMapping("/submit")
	public String userDetails(HttpServletRequest request, Model model)
	{
		String user = request.getParameter("user");
		String pass = request.getParameter("pwd");
		
		User u = new User();
		
		u.setUserName(user);
		u.setPassword(pass);
		
		model.addAttribute("info", u);
		
		return "details";
	}
}
