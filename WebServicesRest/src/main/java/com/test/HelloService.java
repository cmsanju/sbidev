package com.test;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

@Path("/hello")
public class HelloService 
{
	@GET
	@Produces(MediaType.TEXT_PLAIN)
	@Path("/text")
	public String sayHello()
	{
		return "hi this is restfule webservices plain text response";
	}
	
	@GET
	@Produces(MediaType.TEXT_HTML)
	@Path("/html")
	public String sayHelloTextHtml()
	{
		return "<html><body><h3>Hi this is text html response</h3></body></html>";
	}
}
/*

	1. GET -> READ 
	2. POST -> CREATE 
	3. PUT -> UPDATE
	4. DELETE -> DELETE

*/